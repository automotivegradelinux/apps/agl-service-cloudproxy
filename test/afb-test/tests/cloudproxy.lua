--[[
 Copyright 2020 MERA

 author:Leonid Lazarev <leonid.lazarev@orioninc.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
--]]

_AFT.testVerbStatusSuccess('testSendMessageSuccess','cloudproxy','ping', {})
_AFT.testVerbStatusSuccess('testSubscribeToMsgConfirmation','cloudproxy','subscribe', {event="sendMessageConfirmation"})
_AFT.testVerbStatusSuccess('testSubscribeToReceivedMsg','cloudproxy','subscribe', {event="receivedMessage"})
_AFT.testVerbStatusSuccess('testUnsubscribeToUnkownMsg','cloudproxy','unsubscribe', {event="UnknownMsg"}) -- NOTE: Implementation detail - unknown msg are ignored.
_AFT.testVerbStatusError('testSubscribeToUnkownMsg','cloudproxy','subscribe', {event="UnknownMsg"})
_AFT.testVerbStatusSuccess('testUnsubscribeToMsgConfirmation','cloudproxy','unsubscribe', {event="sendMessageConfirmation"})
_AFT.testVerbStatusSuccess('testUnsubscribeToReceivedMsg','cloudproxy','unsubscribe', {event="receivedMessage"})

