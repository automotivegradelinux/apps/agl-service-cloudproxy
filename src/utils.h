/*
 * Copyright (C) 2020 MERA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <functional>
#include <string>

#include <afb/afb-binding.h>

namespace utils
{
    class scope_exit
    {
    public:
        explicit scope_exit(std::function<void()> func)
            : m_f(func)
        {}
        ~scope_exit()
        {
            if (!!m_f)
                m_f();
        }

    private:
        std::function<void()> m_f;
    };

    inline std::string get_application_id(const afb_req_t request)
    {
        char *app_id = afb_req_get_application_id(request);
        std::string appid;
        if(app_id)
        {
            appid = std::string(app_id);
            free(app_id);
        }

        return appid;
    }
}
