/*
 * Copyright (C) 2020 MERA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <afb/afb-binding.h>

#include <map>
#include <unordered_set>
#include <mutex>
#include <string>

class ClientManager
{
public:
    struct ClientCtx
    {
        std::string appid;
        afb_event_t event;
        std::unordered_set<std::string> subs_events{};
        bool subscription{false};
    };

    ClientManager() = default;
    ~ClientManager();
    ClientManager(const ClientManager&) = delete;
    void operator=(const ClientManager&) = delete;

    bool handleRequest(afb_req_t request, const std::string& verb, const std::string& appid);
    bool emitReceivedMessage(const std::string& appid, const std::string& cloud_type, const std::string& data);
    bool emitSendMessageConfirmation(const std::string& appid,  const std::string& cloud_type, bool result);

    static ClientManager& instance();

    // don't use it directly
    static void cbRemoveClientCtx(void *data);

protected:
    ClientCtx* addClient(afb_req_t req, const std::string& appid);
    void removeClient(ClientCtx* ctx);
    bool isSupportedEvent(const std::string& event);

protected:
    std::mutex m_mutex;
    std::map<std::string, ClientCtx*> m_clients;
};
