/*
 * Copyright (C) 2020 MERA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "CloudClient.h"

#include <memory>


struct AzureClientConfig
{
    bool enabled{false};
    std::string device_connection_string;
};

typedef struct IOTHUB_CLIENT_CORE_INSTANCE_TAG* IOTHUB_CLIENT_CORE_HANDLE;
typedef IOTHUB_CLIENT_CORE_HANDLE IOTHUB_DEVICE_CLIENT_HANDLE;

class AzureClient : public CloudClient
{
public:
    AzureClient();
    ~AzureClient();

    bool sendMessage(const std::string& appid, const std::string& data) override;
    bool createConnection() override;

    bool enabled() const override;
    bool connected() const override;
    bool loadConf(GKeyFile* conf_file) override;

    const AzureClientConfig& conf() const;

    static std::pair<std::string, std::string> parseMessage(const std::string& msg);
    static std::string getApplicationId(const std::string& msg);

protected:
    std::unique_ptr<IOTHUB_DEVICE_CLIENT_HANDLE> m_azure_client;
    AzureClientConfig m_conf;
    bool m_iot_inited{false};
};
