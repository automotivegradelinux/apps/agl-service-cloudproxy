/*
 * Copyright (C) 2020 MERA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <string>

typedef struct _GKeyFile GKeyFile;

class CloudClient
{
public:
    CloudClient() = default;
    virtual ~CloudClient() = default;
    CloudClient(const CloudClient&) = delete;
    CloudClient& operator= (const CloudClient&) = delete;

    virtual bool sendMessage(const std::string& appid, const std::string& data) = 0;
    virtual bool createConnection() = 0;

    virtual bool enabled() const = 0;
    virtual bool connected() const = 0;
    virtual bool loadConf(GKeyFile* conf_file) = 0;
};
