/*
 * Copyright (C) 2020 MERA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "CloudClient.h"
#include <string>
#include <thread>
#include <atomic>
#include <deque>
#include <mutex>


struct AwsClientConfig
{
    bool enabled{false};
    std::string mqtt_host;
    uint16_t    mqtt_port{433};
    std::string mqtt_client_id;
    std::string mqtt_out_topic;
    std::string mqtt_in_topic;
    std::string thing_name;
    std::string root_ca_cert_path;
    std::string device_cert_path;
    std::string device_priv_key_path;
};

typedef struct _Client AWS_IoT_Client;

class AwsClient : public CloudClient
{
public:
    static constexpr size_t MAX_QUEUE_SIZE{10000};

    AwsClient();
    ~AwsClient();

    bool sendMessage(const std::string& appid, const std::string& data) override;
    bool createConnection() override;

    bool enabled() const override;
    bool connected() const override;
    bool loadConf(GKeyFile* conf_file) override;

    const AwsClientConfig& conf() const;

    static std::pair<std::string, std::string> parseMessage(const std::string& msg);
    static std::string getApplicationId(const std::string& msg);

protected:
    void main_loop();
    void start();
    void stop();

protected:
    std::unique_ptr<AWS_IoT_Client> m_aws_client;
    AwsClientConfig m_conf;
    std::thread m_thread;
    std::atomic_bool m_thread_stop{false};
    std::atomic_bool m_started{false};
    bool m_connected{false};
    std::deque<std::string> m_queue;
    std::mutex m_mutex;
};
