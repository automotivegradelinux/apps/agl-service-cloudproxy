# Cloud Proxy Service

## Overview

*Cloud proxy service* provides possibility to send and receive data to/from clouds.

It is allowed to use *Cloud proxy service* from several application.
The messages/events separtion between application is done within service.

## Verbs

```
| Name               | Description                             | Data                                                |
| ------------------ |:----------------------------------------|:----------------------------------------------------|
| sendMessage        | send data to the cloud                  | application ID, JSON {"data":"message content"}     |
| subscribe          | subs. for particular application id     | application ID extracted from msg                   |
| unsubscribe        | unsubs. for particular application id   | application ID extracted from msg                   |
| ping               | verify connectivity to service          | N/A                                                 |
```
## Events

```
| Name                         | Description                                             |
| ---------------------------- |:--------------------------------------------------------|
| sendMessageConfirmation      | confirmation from cloud for previously sent message     |
| receivedMessage              | message from cloud to be propagated to the application  |
```

## Configuration

Cloud proxy service requires configuration of the cloud to be used. Configuration is stored in the **config.ini** file.

By default the configuration file **config.ini** is searched stored in

* $AFM_APP_INSTALL_DIR/etc/config.ini  - **default one**
* /etc/cloudproxy-service/etc/config.ini

At the moment only Azure cloud is supported.

### Azure configuration
The configuration has to contain the connection string for device: **DeviceConnectionString** in
section **[AzureCloudConnection]**

```
[AzureCloudConnection]
DeviceConnectionString=My=Azure=Device=Connection=String
```



